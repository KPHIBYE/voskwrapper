﻿using Newtonsoft.Json;

namespace KPB.VoskWrapper
{
    internal sealed class VoskAlternatives
    {
        public VoskResult[] Alternatives { get; set; }

        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
