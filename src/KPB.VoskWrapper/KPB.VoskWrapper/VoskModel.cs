﻿using System;
using System.IO;
using System.Threading.Tasks;

using Vosk;

namespace KPB.VoskWrapper
{
    /// <summary>
    /// <para>A wrapper around the Vosk <see cref="Vosk.Model"/> class that ensures that a model is not used once it has been disposed</para>
    /// Note: One <see cref="VoskModel"/> instance can be used to instantiate multiple <see cref="VoskWrapper"/> instances
    /// </summary>
    public class VoskModel : IDisposable
    {
        private bool disposed = false;
        internal readonly object disposeLock = new object();

        private readonly Model model;
        internal Model Model => disposed ? throw new ObjectDisposedException(nameof(VoskModel)) : model;

        static VoskModel()
        {
            Vosk.Vosk.SetLogLevel(-1);
        }

        private VoskModel(string path)
        {
            model = new Model(path);
        }

        /// <summary>
        /// Asynchronously creates a <see cref="VoskModel"/> using the specified model
        /// </summary>
        /// <param name="path">The path to the Vosk model folder of the model that should be used</param>
        /// <returns>A <see cref="Task{TResult}"/> which, when completed, asynchronously returns a <see cref="VoskModel"/></returns>
        public static Task<VoskModel> CreateAsync(string path)
        {
            if (!Directory.Exists(path)) throw new DirectoryNotFoundException($"The specified model folder \"{path}\" could not be found");

            return Task.Run(() => new VoskModel(path));
        }

        /// <summary>
        /// Clean up the model when it goes away
        /// </summary>
        ~VoskModel()
        {
            Dispose(disposing: false);
        }

        /// <summary>
        /// Releases all resources used by the <see cref="VoskModel"/> object
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the <see cref="VoskModel"/> object
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            lock (disposeLock)
            {
                if (disposed) return;

                if (disposing)
                {
                    // Dispose managed resources
                    model.Dispose();
                }

                // Dispose unmanaged resources

                disposed = true;
            }
        }
    }
}
