﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Vosk;
using Newtonsoft.Json;
#if WINDOWS_RUNTIME
using NAudio.Wave;
#endif

namespace KPB.VoskWrapper
{
    /// <summary>
    /// A wrapper around the Vosk and NAudio libraries that provides live and file based speech to text processing 
    /// </summary>
    public class VoskWrapper : IDisposable
    {
        private bool disposed = false;
        private volatile bool disposingRequested = false;
        internal readonly object disposeLock = new object();

        private readonly AutoResetEvent resetEvent = new AutoResetEvent(true);
        private readonly VoskRecognizer vRec;
        private readonly int sampleRate;
        private readonly int maxAlternatives;
#if WINDOWS_RUNTIME
        private readonly object audioSourceLock = new object();
        private IWaveIn audioSource = null;


        /// <summary>
        /// Gets the number of input devices that are available for live speech to text processing
        /// </summary>
        public static int InputDeviceCount
        {
            get
            {
                PlatformGuard();
                return WaveInEvent.DeviceCount;
            }
        }

        /// <summary>
        /// Gets all currently detected input devices
        /// </summary>
        public static IEnumerable<(int DeviceNumber, string ProductName, int Channels, Guid DeviceNameGuid, Guid ProductNameGuid, Guid ManufacturerGuid)> InputDevices
        {
            get
            {
                PlatformGuard();
                for (int deviceNumber = 0; deviceNumber < InputDeviceCount; ++deviceNumber)
                {
                    var device = WaveInEvent.GetCapabilities(deviceNumber);
                    yield return (deviceNumber, device.ProductName, device.Channels, device.NameGuid, device.ProductGuid, device.ManufacturerGuid);
                }
            }
        }

        /// <summary>
        /// <para>This event will be fired each time a new complete speech recognition result was generated from live audio</para>
        /// <para>Note: Callbacks will be executed on a background thread</para>
        /// Note: <see cref="VoskResult"/> with empty text will be discarded
        /// </summary>
        public event Action<VoskResult[]> ResultAvailable;
#endif

        /// <summary>
        /// Gets a value indicating the execution status of this <see cref="VoskWrapper"/> instance
        /// </summary>
        /// <returns>true if this <see cref="VoskWrapper"/> currently performs speech to text processing; otherwise false</returns>
        public bool IsProcessing
        {
            get
            {
                lock (disposeLock)
                {
                    if (disposed) throw new ObjectDisposedException(nameof(VoskWrapper));
                    bool gotSignal = resetEvent.WaitOne(0);
                    if (gotSignal) resetEvent.Set();
                    return !gotSignal;
                }
            }
        }

        private VoskWrapper(VoskModel vModel, int sampleRate, int maxAlternatives, bool returnWords, string[] grammar)
        {
            // ensures that the model is not disposed during recognizer initialization
            lock (vModel.disposeLock)
            {
                vRec = grammar?.Length > 0 ? new VoskRecognizer(vModel.Model, sampleRate, $"[\"{string.Join("\",\"", grammar)}\"]") : new VoskRecognizer(vModel.Model, sampleRate);
            }

            vRec.SetMaxAlternatives(maxAlternatives);
            vRec.SetWords(returnWords);

            this.sampleRate = sampleRate;
            this.maxAlternatives = maxAlternatives;
        }

        /// <summary>
        /// Asynchronously creates a <see cref="VoskWrapper"/> using the specified model, sample rate, max. alternatives, if individual words should be returned and grammar
        /// </summary>
        /// <param name="vModel">
        /// <para>The model that should be used</para>
        /// Note: A <see cref="VoskModel"/> instance can be reused and simultaneously used by multiple <see cref="VoskWrapper"/> instances</param>
        /// <param name="sampleRate">
        /// <para>The sample rate that the model and recording device will use in Hz</para>
        /// Note: The recognition quality will be too low with a sample rate of less than 1000. Choose at least 4000 for acceptable results.</param>
        /// <param name="maxAlternatives">
        /// <para>The maximum number of alternative interpretations of the spoken sounds that the model should produce</para>
        /// Note: If zero is chosen, the <see cref="VoskResult"/> will not contain confidence, but <see cref="VoskWordResult"/> will</param>
        /// <param name="returnWords">
        /// <para>Whether or not individual words with their timestamps and confidence should be returned</para>
        /// Note: <see cref="VoskWordResult"/> will only contain confidence values if <paramref name="maxAlternatives"/> is set to zero</param>
        /// <param name="grammar">An array of strings that contains the whole grammar that the model will use eg. ["hugo", "hallo", "stop", "tonne", "mülltonne", "[unk]"] where [unk] is the unknown token</param>
        /// <returns>A <see cref="Task{TResult}"/> which, when completed, asynchronously returns a <see cref="VoskWrapper"/></returns>
        public static Task<VoskWrapper> CreateAsync(VoskModel vModel, int sampleRate = 16_000, int maxAlternatives = 0, bool returnWords = false, string[] grammar = null)
        {
            if (sampleRate < 10) throw new ArgumentException("A sample rate below 10 is not supported by NAudio", nameof(sampleRate));
            if (maxAlternatives < 0) throw new ArgumentException("Invalid number of alternatives provided", nameof(maxAlternatives));

            return Task.Run(() => new VoskWrapper(vModel, sampleRate, maxAlternatives, returnWords, grammar));
        }

#if WINDOWS_RUNTIME
        /// <summary>
        /// <para>Starts live speech to text processing using the specified input device and sample rate that was provided in the constructor</para>
        /// Note: If the specified device is disconnected while live text to speech processing is being performed, the processing does not stop and no exception will be thrown but no new data will be processed
        /// </summary>
        /// <param name="inputDevice">The device number of the input device that should be used. All available devices can be retrieved from <see cref="InputDevices"/></param>
        public void StartLiveProcessing(int inputDevice)
        {
            PlatformGuard();

            lock (disposeLock)
            {
                if (disposed) throw new ObjectDisposedException(nameof(VoskWrapper));
                if (InputDeviceCount < 1) throw new InvalidOperationException("No audio sources are available for live recording");
                if (inputDevice < 0 || InputDeviceCount <= inputDevice) throw new ArgumentException("The selected audio source is not available", nameof(inputDevice));
                if (!resetEvent.WaitOne(0)) throw new InvalidOperationException("Speech to text processing is currently being performed on this instance");

                lock (audioSourceLock)
                {
                    try
                    {
                        audioSource = new WaveInEvent() { DeviceNumber = inputDevice, WaveFormat = new WaveFormat(sampleRate, 1) };
                        audioSource.DataAvailable += AudioDataAvailableHandler;
                        audioSource.StartRecording();
                    }
                    catch
                    {
                        if (audioSource is null)
                        {
                            resetEvent.Set();
                        }

                        StopLiveProcessing();

                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Note: Will be executed on a background thread
        /// </summary>
        private void AudioDataAvailableHandler(object sender, WaveInEventArgs args)
        {
            // prevent access to args.Buffer when audioSource is already disposed
            lock (audioSourceLock)
            {
                if (audioSource is null) return;
                if (!vRec.AcceptWaveform(args.Buffer, args.BytesRecorded)) return;
            }

            var result = ParseResult(vRec.Result());
            if (result.Length == 0) return;
            ResultAvailable?.Invoke(result);
        }

        /// <summary>
        /// Stops live speech to text processing
        /// </summary>
        public void StopLiveProcessing()
        {
            lock (audioSourceLock)
            {
                if (audioSource is null) return;

                try
                {
                    audioSource.StopRecording();
                    audioSource.DataAvailable -= AudioDataAvailableHandler;
                    audioSource.Dispose();
                }
                finally
                {
                    audioSource = null;
                    vRec.Reset();
                    resetEvent.Set();
                }
            }
        }

        private static void PlatformGuard()
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) throw new PlatformNotSupportedException("This operation is only supported on Windows");
        }
#endif

        /// <summary>
        /// <para>Performs speech to text processing on a WAV file</para>
        /// <para>Note: The WAV files sample rate has to match the one provided in the constructor</para>
        /// Note: <see cref="VoskResult"/> with empty text will be discarded, thus an empty array may be returned
        /// </summary>
        /// <param name="path">The path to the WAV file on which speech to text processing should be performed</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the processing. If a relevant result can be generated from the already processed data it will be returned, else the returned array will be empty.</param>
        /// <returns>An asynchronous task that completes once the whole file has been processed, or the operation was cancelled with a <see cref="VoskResult"/> array containing all relevant speech recognition results</returns>
        public Task<VoskResult[]> ProcessFileAsync(string path, CancellationToken cancellationToken = default)
        {
            lock (disposeLock)
            {
                if (disposed) throw new ObjectDisposedException(nameof(VoskWrapper));
                if (!File.Exists(path)) throw new FileNotFoundException($"The file \"{path}\" could not be found", Path.GetFileName(path));
                if (!resetEvent.WaitOne(0)) throw new InvalidOperationException("Speech to text processing is currently being performed on this instance");
            }

            return Task.Run(() =>
            {
                using (Stream source = File.OpenRead(path))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while (!cancellationToken.IsCancellationRequested && !disposingRequested && (bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        vRec.AcceptWaveform(buffer, bytesRead);
                    }
                }

                return ParseResult(vRec.FinalResult());

            }, cancellationToken)
            .ContinueWith(t =>
            {
                resetEvent.Set();
                return t.IsCanceled || t.IsFaulted ? Array.Empty<VoskResult>() : t.Result;
            });
        }

        private VoskResult[] ParseResult(string resultJson)
        {
            // Text should only ever be empty, but we also check for null just to be sure

            if (maxAlternatives == 0)
            {
                var result = JsonConvert.DeserializeObject<VoskResult>(resultJson);
                return string.IsNullOrEmpty(result.Text) ? Array.Empty<VoskResult>() : new[] { result };
            }

            VoskResult[] results = JsonConvert.DeserializeObject<VoskAlternatives>(resultJson).Alternatives;

            FilterArrayInPlace(ref results, vr => !string.IsNullOrEmpty(vr.Text));

            return results;
        }

        /// <summary>
        /// Moves all elements that satisfy a predicate to the front of the array and replaces it with a resized version.
        /// If all elements satisfy the predicate, the array is not changed.
        /// </summary>
        private static void FilterArrayInPlace<T>(ref T[] arr, Func<T, bool> predicate)
        {
            int okCount = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                T t = arr[i];
                if (predicate(t))
                {
                    arr[okCount++] = t;
                }
            }

            if (okCount == 0)
            {
                arr = Array.Empty<T>();
            }
            else if (okCount < arr.Length)
            {
                arr = new ArraySegment<T>(arr, 0, okCount).ToArray();
            }

            // if okCount == arr.Length, do nothing
        }

        /// <summary>
        /// Clean up the wrapper when it goes away
        /// </summary>
        ~VoskWrapper()
        {
            Dispose(disposing: false);
        }

        /// <summary>
        /// Releases all resources used by the <see cref="VoskWrapper"/> object
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the <see cref="VoskWrapper"/> object
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            lock (disposeLock)
            {
                if (disposed) return;

                disposingRequested = true;
#if WINDOWS_RUNTIME
                StopLiveProcessing();
#endif
                resetEvent.WaitOne(); // wait for processing to finish

                if (disposing)
                {
                    // dispose managed resources
                    vRec.Dispose();
                    resetEvent.Dispose();
                }

                // dispose unmanaged resources

                disposed = true;
            }
        }
    }
}
