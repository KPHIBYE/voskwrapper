# VoskWrapper

[![License](https://img.shields.io/gitlab/license/37437942?color=blue)](https://gitlab.com/KPHIBYE/voskwrapper/-/blob/main/LICENSE)
[![NuGet version (KPB.VoskWrapper)](https://img.shields.io/nuget/v/KPB.VoskWrapper?color=blue)](https://www.nuget.org/packages/KPB.VoskWrapper)

This library is intended for people that want to quickly get startet with the [Vosk speech recognition toolkit](https://github.com/alphacep/vosk-api) in a .NET environment and find the [official examples](https://github.com/alphacep/vosk-api/tree/master/csharp/demo) insufficient or too complicated for live processing workloads. It should be thread safe, but I don't want to advertise it as such since I am not completely certain that deadlocks and race conditions are impossible.

For live speech to text processing, [NAudio](https://github.com/naudio/NAudio) is used. This means that although this is a `.NET Standard 2.0` library, it depends on **Windows specific features** for this functionality.

I hope that the code itself and the documentation comments prove to be helpful in understanding and working with the Vosk library, since I find some behavior of it very unintuitive or badly documented and I have either addressed or explained these things in this library.

[Changelog](https://gitlab.com/KPHIBYE/voskwrapper/-/blob/main/CHANGELOG)

## Usage
### Enumerate available input devices
```csharp
using KPB.VoskWrapper;

foreach (var device in VoskWrapper.InputDevices)
{
    Console.WriteLine(device);
}
```
> (0, Mikrofon (Realtek High Definiti, 2, 00000000-0000-0000-0000-000000000000, e36dc311-6d9a-11d1-a21a-00a0c9223196, d5a47fa8-6d98-11d1-a21a-00a0c9223196)

### Start live speech to text processing
```csharp
using var model = await VoskModel.CreateAsync(@"models\vosk-model-en-us-0.22-lgraph");
using var vw = await VoskWrapper.CreateAsync(model, sampleRate: 8000, maxAlternatives: 0, returnWords: false, grammar: new[] { "toast", "test", "[unk]" });

vw.ResultAvailable += res =>
{
    foreach (var result in res)
    {
        Console.WriteLine(result.Text);
    }
};

Console.WriteLine(vw.IsProcessing);
vw.StartLiveProcessing(inputDevice: 0);
Console.WriteLine(vw.IsProcessing);
```
> False

> True

> [unk] toast test [unk] [unk]

### Process a WAV file (output assuming no grammar has been specified)
```csharp
var res = await vw.ProcessFileAsync(@"samples\male_8khz.wav");

Console.WriteLine(res[0].Text);
```
> but what if somebody decides to break it be careful that you keep adequate coverage but look for places to save money ...

## Support & Contributing
If you have any issues with this library or want to make a suggestion for an improvement, feel free to create an [issue](https://gitlab.com/KPHIBYE/voskwrapper/issues). If you want to take things into your own hands, feel free to create a [merge request](https://gitlab.com/KPHIBYE/voskwrapper/merge_requests). Just try to keep the code style similar to mine.
